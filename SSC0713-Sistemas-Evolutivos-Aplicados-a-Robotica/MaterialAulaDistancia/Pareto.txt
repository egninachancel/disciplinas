A- Pareto efficiency
- Conceitos Gerais  -  https://en.wikipedia.org/wiki/Pareto_efficiency

B- Otimização Multiobjetivo - https://ppgee.ufmg.br/defesas/1041M.PDF
1) Vários conceitos interessantes apartir da pag. 30

2) Exemplo de mapeamento do espaço de decisão para o espaço de objetivos - pag. 32

3) Soluções Pareto-ótimas e Dominância Pareto - pag 33

4) Cone de dominancia - pag 34

5) Métodos Clássicos de Otimização Multiobjetivo - pag 35

6) Soma ponderada dos objetivos - pag 37

7) E-restrito - pag 38

8) Métodos Evolutivos - pag 40
- Várias ideias de como calcular o fitness e selecionar para ocrossover


C- MÉTODOS DE OTIMIZAÇÃO MULTIOBJETIVO E DESIMULAÇÃO APLICADOS AO PROBLEMA DE PLANEJAMENTO OPERACIONAL DE LAVRA EM MINAS A CÉU ABERTO
- http://www.decom.ufop.br/prof/marcone/Disciplinas/POMineracao/arquivos/GuidoPantuza_Dissertacao.pdf

1) Otimização Multiobjetivo  pg 11

2) Técnicas Heurísticas Multiobjetivo pg 20

3) Metaheurísticas pg 22

4) Algoritmo Genético Multiobjetivoc pg 23
- Várias ideias de como calcular o fitness e selecionar para ocrossover



D- Métodos de Otimizacao Multiobjetivo  - https://www.teses.usp.br/teses/disponiveis/3/3143/tde-19112004-165342/publico/Kleber-Cap3.pdf
